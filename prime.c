#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

bool is_prime(unsigned long num) {
    if (num <= 1) {
        return false;
    }
    if(num == 2) return true;
    if(num % 2 == 0) return false;
    for (int i = 3; i <= sqrt((double)num); i = i+2) {
        if (num % i == 0) return false;
    }
    return true;
}

bool is_digit(char *string) {
    for (int i = 0; string[i] != '\0'; i++) {
         if(!isdigit(string[i])) {
                return false;
         }
    }
    return true;
}

int main() {
    unsigned long input;
    char string[20];
    printf("Enter a number\n");
    scanf("%s", string);
    if(is_digit(string)) {
        input = atoi(string);
        printf("%ld is %sprime\n", input, is_prime(input) ? "" : "not ");
    } else {
        printf("%s is not a natural number\n", string);
    }
    return 0;
}
