#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <stdlib.h>


int array1[4] = {2,5,6,8};
int array2[4] = {5,2,4,1};
long scalar_product(int *vector1, int *vector2){
    long result = 0;
    for(int i = 0; i <= sizeof(vector1)/sizeof(int); i++) {
        result+= vector1[i]*vector2[i];
    }
    return result;
}

int main() {
    printf("%lu\n",scalar_product(array1, array2));
    return 0;
}

